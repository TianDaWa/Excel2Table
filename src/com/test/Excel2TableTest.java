package com.test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;
import util.DBTool;

import com.service.Excel2Table;

public class Excel2TableTest extends TestCase {

	public void test() throws Exception{
		Excel2Table e2t=new Excel2Table();
		InputStream is = new FileInputStream("D:\\add.xls");
		List<String> list= e2t.createTableByExcel(is);
		List<Map<Integer, String>> listData = e2t.insertDataToTable(is);
		
		DBTool db=new DBTool();
		for (int i = 0; i < list.size(); i++) {
			db.execSql(list.get(i));
		}
		for (int i = 0; i < listData.size(); i++) {
			Map<Integer, String> sql=listData.get(i);
			for (int j = 1; j <= sql.size(); j++) {
				db.execSql((String) sql.get(j));
			}
		}
	}
}
