# Excel2Table

## 环境：
+ Java版本：jdk1.7+；
+ MySQL5.6；
+ Excel：97——03版本；若要支持“.xlsx”格式的Excel，需再加一个excel2table类，只需在原基础上修改引入的包及变量名即可。
+ 插件：Apache-poi。

## 说明
将lib里的jar包下载下来之后，在项目里面通过右击项目名称->Build Path->Configure Build Path->Libraries->Add Libraries->User Library,新建一个library，将jar包添加进去。